# Changelog

## v2.3.2
- Updated `vue-loader` to v15 and refactor tests

## v2.3.1
- Updated Macbeth assets
- Updated styling for Conclusion component
- Removed Blender Pro fonts; replaced by Jua
- Fixed padding issues with dialogue boxes
- Fixed audio issue on Lady Macbeth component

## v2.3.0 
- Added mute button for background music
- Removed Palmistry resources since it is now split into another package
- Fixed site not opening locally via `index.html`
- Removed lint testing in Gitlab CI
- Updated assets according to client's request
- Removed Palmistry component and assets; see 3c92889

## v2.2.2
- Added back button to components with dialogues
- Fixed an alignment on Webkit browsers

## v2.1.1
- Added a concerned expression/emotion to the teacher in Macbeth component
- Removed left or right styling for speech bubble
- Added live links from Literacy Times to their individual sections

## v2.1.0
- Optimised route navigation for Newspaper component

## v2.0.1
- Fixed sequence of responses amongst teacher and students
- Added style for speech bubbles
- Added function to play tones in sequence before proceeding to the next page
- Added global Back Button
- Upgraded Font Awesome to v5

## v2.0.0
- Upgraded Webpack to v4

## v1.1.1
- Updated assets

## v1.1.0
- Added Palmistry component

## v1.0.0
### Initial Release
