import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Headline from '@/components/Headline'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Headline', () => {
  let wrapper, store, state, computed

  beforeEach(() => {
    state = {
      selectedHeadline: {
        id: 1,
        title: 'Family man perishes in seven car pile-up',
        button: 'Road Accident',
        selected: false,
        dialogues: [
          {
            speaker: 'student',
            text: 'In headline no. 1, the words ”family man” reminds us that he has a wife and kids who will miss him, which conveys a certain tragic and sad tone in the reporting of the accident.'
          }
        ]
      }
    }

    store = new Vuex.Store({
      state
    })

    computed = {
      dialogues: () => state.selectedHeadline.dialogues
    }

    wrapper = shallowMount(Headline, {
      store, localVue, computed
    })

    wrapper.setData({ dialogueIndex: 0 })
  })

  test('should navigate to the respective newspaper headlines when newspaper is clicked', () => {
    const $route = {
      path: '/newspaper-headlines'
    }

    const router = new VueRouter()

    wrapper = shallowMount(Headline, {
      store, router, localVue, computed
    })

    wrapper.find('.is-back').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
