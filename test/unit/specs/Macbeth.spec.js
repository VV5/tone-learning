import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Macbeth from '@/components/Macbeth'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Macbeth', () => {
  let wrapper, store, state, mutations

  beforeEach(() => {
    state = {}

    mutations = {
      hasAudio: jest.fn(),
      setMacbethFinished: jest.fn()
    }

    store = new Vuex.Store({
      state, mutations
    })

    wrapper = shallowMount(Macbeth, {
      store, localVue
    })

    wrapper.setData({
      sets: [
        {
          id: 1,
          text: [
            { paragraph: 'Foo' },
            { paragraph: 'Bar' }
          ]
        },
        {
          id: 2,
          text: [
            { paragraph: 'Hello' },
            { paragraph: 'World' }
          ]
        }
      ]
    })
  })

  test('should decrement textIndex when back button is clicked', () => {
    wrapper.setData({ setIndex: 1, textIndex: 1 })

    expect(wrapper.vm.setIndex).toBe(1)
    expect(wrapper.vm.textIndex).toBe(1)

    wrapper.find('.is-previous').trigger('click')

    expect(wrapper.vm.setIndex).toBe(1)
    expect(wrapper.vm.textIndex).toBe(0)
  })

  test('should reset textIndex to 0 when back button is clicked', () => {
    wrapper.setData({ setIndex: 1 })

    expect(wrapper.vm.setIndex).toBe(1)

    wrapper.find('.is-previous').trigger('click')

    expect(wrapper.vm.setIndex).toBe(0)
    expect(wrapper.vm.textIndex).toBe(0)
  })

  test('should increment set index and text index correctly when next button is clicked', () => {
    expect(wrapper.vm.setIndex).toBe(0)
    expect(wrapper.vm.textIndex).toBe(0)

    const nextButton = wrapper.find('.is-next')
    nextButton.trigger('click')

    expect(wrapper.vm.textIndex).toBe(1)
    expect(wrapper.vm.sets[0].text[1].paragraph).toBe('Bar')

    nextButton.trigger('click')

    expect(wrapper.vm.setIndex).toBe(1)
    expect(wrapper.vm.textIndex).toBe(0)
  })

  test('should navigate back to Intonation when button is clicked', () => {
    wrapper.setData({ setIndex: 1, textIndex: 1 })

    const $route = {
      path: '/intonation'
    }

    const router = new VueRouter()

    wrapper = shallowMount(Macbeth, {
      router, localVue
    })

    wrapper.find('.goto-intonation').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })

  test('should navigate to Lady Macbeth when button is clicked', () => {
    const $route = {
      path: '/lady-macbeth'
    }

    const router = new VueRouter()

    wrapper = shallowMount(Macbeth, {
      store,
      router,
      localVue,
      computed: {
        isReady: () => true
      }
    })

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
