import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Conclusion from '@/components/Conclusion'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Conclusion', () => {
  let wrapper, store, state

  beforeEach(() => {
    state = {}

    store = new Vuex.Store({
      state
    })

    wrapper = shallowMount(Conclusion, {
      store, localVue
    })
  })

  test('should navigate to newspaper when button is clicked', () => {
    const $route = {
      path: '/newspaper'
    }

    const router = new VueRouter()

    wrapper = shallowMount(Conclusion, {
      store, router, localVue,
      computed: {
        newspaperFinished: () => false
      }

    })

    wrapper.find('.goto-newspaper').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })

  test('should navigate to choices when button is clicked', () => {
    const $route = {
      path: '/choice'
    }

    const router = new VueRouter()

    wrapper = shallowMount(Conclusion, {
      store, router, localVue
    })

    wrapper.find('.goto-choices').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
