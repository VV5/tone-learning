import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Router from 'vue-router'
import Newspaper from '@/components/Newspaper'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(Router)

describe('Newspaper', () => {
  let wrapper, store, state, mutations, getters

  beforeEach(() => {
    state = {
      headlines: [
        { id: 1, selected: false },
        { id: 2, selected: false }
      ],
      showNewspaper: false
    }

    mutations = {
      setSelectedHeadline: jest.fn(),
      setShowChoices: jest.fn()
    }

    getters = {
      showNextButton: () => state.headlines.every(headline => headline.selected === true)
    }

    store = new Vuex.Store({
      state, mutations, getters
    })

    wrapper = shallowMount(Newspaper, {
      store, localVue
    })
  })

  test('should navigate to the correct headline', () => {
    const $route = {
      path: '/newspaper-headlines'
    }

    const router = new Router()

    wrapper = shallowMount(Newspaper, {
      store, router, localVue
    })

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })

  test('should navigate to responses when all conditions are met', () => {
    const $route = {
      path: '/choice'
    }

    const router = new Router()

    wrapper = shallowMount(Newspaper, {
      store,
      router,
      localVue,
      computed: {
        showNewspaper: () => true,
        showNextButton: () => true
      }
    })

    wrapper.find('.goto-choices').trigger('click')

    expect(mutations.setShowChoices).toHaveBeenCalled()

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
