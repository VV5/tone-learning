import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Intonation from '@/components/Intonation'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Intonation', () => {
  let wrapper, store, state
  let router

  beforeEach(() => {
    state = {}

    store = new Vuex.Store({
      state
    })

    router = new VueRouter()

    wrapper = shallowMount(Intonation, {
      store, router, localVue
    })

    wrapper.setData({
      sets: [
        {
          id: 1
        },
        {
          id: 2,
          text: [
            { paragraph: 'Foo' },
            { paragraph: 'Bar' }
          ]
        },
        {
          id: 3,
          text: [
            { paragraph: 'Hello' },
            { paragraph: 'World' }
          ]
        }
      ],
      setIndex: 0,
      textIndex: 0
    })
  })

  test('should decrement set index when previous button is clicked', () => {
    wrapper.setData({
      setIndex: 2,
      textIndex: 1
    })

    expect(wrapper.vm.sets[2].text[1].paragraph).toBe('World')

    const previousButton = wrapper.find('.is-previous')
    previousButton.trigger('click')

    expect(wrapper.vm.textIndex).toBe(0)
    expect(wrapper.vm.sets[2].text[0].paragraph).toBe('Hello')

    previousButton.trigger('click')

    expect(wrapper.vm.setIndex).toBe(1)
    expect(wrapper.vm.sets[1].text[0].paragraph).toBe('Foo')
  })

  test('should increment set index when next button is clicked', () => {
    expect(wrapper.vm.setIndex).toBe(0)

    const nextButton = wrapper.find('.is-next')
    nextButton.trigger('click')

    expect(wrapper.vm.setIndex).toBe(1)
  })

  test('should reset text index to 0 each time the set index is increased', () => {
    wrapper.setData({ setIndex: 1 })

    expect(wrapper.vm.setIndex).toBe(1)
    expect(wrapper.vm.sets[1].text[0].paragraph).toBe('Foo')
    wrapper.vm.$nextTick()

    const nextButton = wrapper.find('.is-next')
    nextButton.trigger('click')

    expect(wrapper.vm.textIndex).toBe(1)
    expect(wrapper.vm.sets[1].text[1].paragraph).toBe('Bar')

    nextButton.trigger('click')

    expect(wrapper.vm.setIndex).toBe(2)
    expect(wrapper.vm.sets[2].text[0].paragraph).toBe('Hello')

    nextButton.trigger('click')

    expect(wrapper.vm.sets[2].text[1].paragraph).toBe('World')
  })

  test('should navigate correctly when ready', () => {
    const $route = {
      path: '/macbeth'
    }

    const router = new VueRouter()

    wrapper = shallowMount(Intonation, {
      store,
      router,
      localVue,
      computed: {
        isReady: () => true
      }
    })

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })

  test('should navigate to choice when button is clicked', () => {
    const $route = {
      path: '/choice'
    }

    wrapper.find('.goto-choice').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
