import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import LadyMacbeth from '@/components/LadyMacbeth'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('LadyMacbeth', () => {
  let wrapper, store, mutations

  beforeEach(() => {
    mutations = {
      disableAudio: jest.fn(),
      hasAudio: jest.fn()
    }

    store = new Vuex.Store({
      mutations
    })

    wrapper = shallowMount(LadyMacbeth, {
      store, localVue
    })

    window.HTMLMediaElement.prototype.load = () => {}
    window.HTMLMediaElement.prototype.play = () => {}
  })

  test('should increment scene index when button is clicked', () => {
    expect(wrapper.vm.sceneIndex).toBe(0)

    wrapper.find('.is-next').trigger('click')

    expect(mutations.disableAudio).toHaveBeenCalled()
    expect(wrapper.vm.sceneIndex).toBe(1)
  })

  test('should go back to Macbeth when button is clicked', () => {
    const $route = {
      path: '/macbeth'
    }

    const router = new VueRouter()

    wrapper = shallowMount(LadyMacbeth, {
      router, localVue
    })

    wrapper.find('.is-previous').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })

  test('should navigate to conclusion when all next button is clicked after all tones are clicked', () => {
    const $route = {
      path: '/conclusion'
    }

    const router = new VueRouter()

    wrapper = shallowMount(LadyMacbeth, {
      store,
      router,
      localVue,
      computed: {
        allCompleted: () => true
      }
    })

    wrapper.setData({ sceneIndex: 1 })

    wrapper.find('.goto-conclusion').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })

  test('should show text when each image is clicked', () => {
    jest.useFakeTimers()

    wrapper.setData({ sceneIndex: 1 })

    wrapper.findAll('.is-clickable').at(0).trigger('click')

    expect(wrapper.vm.showTeacherDialogue).toBe(true)

    jest.runAllTimers()

    expect(wrapper.vm.showParagraphSecondIndex).toBe(true)
  })
})
