import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Concept from '@/components/Concept'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Concept', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(Concept)

    wrapper.setData({
      slides: [
        { id: 1 }, { id: 2 }
      ]
    })
  })

  test('back button should decrement slide index on clicked', () => {
    wrapper.setData({ slideIndex: 1 })

    expect(wrapper.vm.slideIndex).toBe(1)

    const previousButton = wrapper.find('.is-previous')
    previousButton.trigger('click')

    expect(wrapper.vm.slideIndex).toBe(0)

    previousButton.trigger('click')

    expect(wrapper.vm.slideIndex).toBe(0)
  })

  test('next button should increment slide index on clicked', () => {
    expect(wrapper.vm.slideIndex).toBe(0)

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.slideIndex).toBe(1)
  })

  test('should navigate to choice correctly', () => {
    const $route = {
      path: '/choice'
    }

    const router = new VueRouter()

    const wrapper = shallowMount(Concept, {
      router, localVue
    })

    wrapper.setData({ slideIndex: 2 })

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
