import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Introduction from '@/components/Introduction'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Introduction', () => {
  test('button should navigate to correct path', () => {
    const $route = {
      path: '/concept'
    }

    const router = new VueRouter()

    const wrapper = shallowMount(Introduction, {
      router, localVue
    })

    wrapper.find('button').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
