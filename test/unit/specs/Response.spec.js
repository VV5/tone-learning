import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Response from '@/components/Response'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Response', () => {
  let wrapper, store, mutations, data

  beforeEach(() => {
    data = {
      responses: [
        {
          speaker: 'student-1',
          text: 'Foo',
          teacher: 'teacher',
          textTwo: 'Bar'
        },
        {
          speaker: 'student-1',
          text: 'Hello World!',
          background: 'newspaper-right-cutout-1'
        }
      ],
      responseIndex: 0
    }

    mutations = {
      setNewspaperFinished: jest.fn()
    }

    store = new Vuex.Store({
      mutations
    })

    wrapper = shallowMount(Response, {
      store,
      localVue,
      computed: {
        response: () => data.responses[data.responseIndex]
      }
    })

    wrapper.setData(data)
  })

  test('should decrease response index when previous button is clicked', () => {
    wrapper.setData({ responseIndex: 1 })

    expect(wrapper.vm.responseIndex).toBe(1)

    const previousButton = wrapper.find('.is-previous-response')
    previousButton.trigger('click')

    expect(wrapper.vm.responseIndex).toBe(0)

    previousButton.trigger('click')

    expect(wrapper.vm.responseIndex).toBe(0)
  })

  test('should increase response index when next button is clicked', () => {
    expect(wrapper.vm.responseIndex).toBe(0)

    wrapper.find('.is-next-response').trigger('click')

    expect(wrapper.vm.responseIndex).toBe(1)
  })

  test('should set intonations as completed', () => {
    wrapper.setData({
      isFinished: true,
      responseIndex: 0
    })

    expect(wrapper.vm.responseIndex).toBe(0)

    wrapper.find('.is-next-response').trigger('click')

    wrapper.vm.$emit('setNewspaperFinished')

    expect(wrapper.emitted().setNewspaperFinished).toBeTruthy()
  })

  test('should setNewspaperFinished to true', () => {
    wrapper.setData({ responseIndex: wrapper.vm.responses.length - 1 })

    wrapper.find('.is-next-response').trigger('click')

    expect(wrapper.vm.isFinished).toBe(true)
    expect(mutations.setNewspaperFinished).toHaveBeenCalled()
  })

  test('should navigate to intonation when all responses have been interacted', () => {
    const $route = {
      path: '/intonation'
    }

    const router = new VueRouter()

    wrapper = shallowMount(Response, {
      store, router, localVue, data
    })

    wrapper.setData({
      isFinished: true,
      endSlide: 1
    })

    wrapper.find('.is-next-response').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })

  test('should navigate to choices when button is clicked', () => {
    const $route = {
      path: '/choice'
    }

    const router = new VueRouter()

    wrapper = shallowMount(Response, {
      router,
      localVue,
      computed: {
        macbethFinished: () => true
      }
    })

    wrapper.setData({ endSlide: 2, isFinished: true })

    wrapper.find('.goto-choices').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
