import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Router from 'vue-router'
import NewspaperHeadlines from '@/components/NewspaperHeadlines'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(Router)

describe('NewspaperHeadlines', () => {
  let wrapper, store, state, mutations, getters

  beforeEach(() => {
    state = {
      headlines: [
        { id: 1, selected: false },
        { id: 2, selected: false }
      ]
    }

    mutations = {
      setSelectedHeadline: jest.fn()
    }

    getters = {
      showNextButton: jest.fn()
    }

    store = new Vuex.Store({
      state, mutations, getters
    })

    wrapper = shallowMount(NewspaperHeadlines, {
      store, localVue
    })
  })

  test('should navigate to the correct headline when its respective headline is clicked', () => {
    const router = new Router()

    wrapper = shallowMount(NewspaperHeadlines, {
      store,
      router,
      localVue,
      computed: {
        showNextButton: () => true
      }
    })

    wrapper.findAll('.newspaper-cutout').at(0).trigger('click')

    expect(wrapper.vm.$route.path).toBe(`/headline/${wrapper.vm.headlines[0].id}`)
  })

  test('should navigate to responses', () => {
    const $route = {
      path: '/response'
    }

    const router = new Router()

    wrapper = shallowMount(NewspaperHeadlines, {
      store,
      router,
      localVue,
      computed: {
        showNextButton: () => true
      }
    })

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
