import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Choice from '@/components/Choice'
import DefinitionModal from '@/components/DefinitionModal'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Choice', () => {
  let wrapper, store, state, mutations
  let router

  beforeEach(() => {
    state = {
      showChoices: false
    }

    mutations = {
      setShowChoices: jest.fn()
    }

    store = new Vuex.Store({
      state, mutations
    })

    router = new VueRouter()

    wrapper = shallowMount(Choice, {
      attachToDocument: true,
      store,
      router,
      localVue
    })
  })

  test('should close the modal if ESC key is pressed', () => {
    wrapper.trigger('keyup', {
      key: 'Escape'
    })

    expect(wrapper.vm.isActive).toBe(false)
  })

  test('should not have anything happening when any key is pressed', () => {
    wrapper.setData({ isActive: true })

    wrapper.trigger('keyup')

    expect(wrapper.vm.isActive).toBe(true)
  })

  test('should navigate back to concept if slideIndex is 0', () => {
    const $route = {
      path: '/concept'
    }

    wrapper.find('.is-previous').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })

  test('emitted event from child component should close modal', () => {
    wrapper.find(DefinitionModal).vm.$emit('closeModal')

    expect(wrapper.vm.isActive).toBe(false)
  })

  test('button should open modal to display definition of tone', () => {
    wrapper = shallowMount(Choice, {
      store,
      localVue,
      computed: {
        showChoices: () => true
      }
    })

    wrapper.findAll('.read-button').at(0).trigger('click')

    expect(wrapper.vm.isActive).toBe(true)
  })

  test('button should navigate to newspaper', () => {
    const $route = {
      path: '/newspaper'
    }

    wrapper.findAll('.read-button').at(1).trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
