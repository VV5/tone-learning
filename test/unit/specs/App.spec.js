import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import App from '@/App'
import store from '@/store'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('App', () => {
  let wrapper, router

  beforeEach(() => {
    router = new VueRouter()

    wrapper = shallowMount(App, {
      attachToDocument: true,
      store,
      router,
      localVue
    })
  })

  test('should resize orientation correctly', () => {
    expect(wrapper.vm.isLandscape).toBe(true)

    window.innerWidth = 1080
    window.innerHeight = 1920

    wrapper.trigger('resize')

    expect(wrapper.vm.isLandscape).toBe(false)
  })

  test('should mute background music when button is clicked', () => {
    expect(wrapper.vm.isMuted).toBe(false)

    wrapper.find('.mute-button').trigger('click')

    expect(wrapper.vm.isMuted).toBe(true)
  })
})
