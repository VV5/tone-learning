import { shallowMount } from '@vue/test-utils'
import DefinitionModal from '@/components/DefinitionModal'

describe('DefinitionModal', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(DefinitionModal, {
      propsData: {
        isActive: true,
        selectedChoice: {
          title: 'Foo'
        }
      }
    })
  })

  test('should close modal when click outside of modal', () => {
    wrapper.find('.modal-background').trigger('click')

    wrapper.vm.$emit('closeModal')

    expect(wrapper.emitted().closeModal).toBeTruthy()
  })

  test('should close modal when close button is clicked', () => {
    wrapper.find('.delete').trigger('click')

    wrapper.vm.$emit('closeModal')

    expect(wrapper.emitted().closeModal).toBeTruthy()
  })
})
