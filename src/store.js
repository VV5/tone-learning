import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: {
    isLandscape: true,
    isMuted: false,
    headlines: [
      {

        id: 1,
        title: 'Family man perishes in seven car pile-up',
        button: 'Road Accident',
        complete: false,
        dialogues: [
          {
            speaker: 'student',
            text: 'In this headline, the words <span class="is-bold">"family man"</span> reminds us that he has a wife and kids who will miss him, which conveys a certain tragic and sad tone in the reporting of the accident.'
          }
        ]
      },
      {
        id: 2,
        title: 'Reckless driving ends in unnecessary death',
        button: 'Reckless Driving',
        complete: false,
        dialogues: [
          {
            speaker: 'student',
            text: 'In this headline, the words  <span class="is-bold">"reckless"</span> and  <span class="is-bold">"unnecessary"</span> create a harsh tone that suggests it was the driver’s fault.'
          }
        ]
      },
      {
        id: 3,
        title: 'Poor road conditions possible factor in fatal accident',
        button: 'Bad Weather',
        complete: false,
        dialogues: [
          {
            speaker: 'student',
            text: 'In this headline, the words  <span class="is-bold">"possible factor"</span> make the tone neutral. The headline sounds more objective in this case.'
          }
        ]
      }
    ],
    selectedHeadline: {},
    showChoices: false,
    macbethFinished: false,
    newspaperFinished: false,
    hasAudio: true
  },
  mutations: {
    setIsLandscape (state, status) {
      state.isLandscape = status
    },
    toggleMute (state) {
      state.isMuted = !state.isMuted
    },
    setSelectedHeadline (state, headline) {
      const index = state.headlines.indexOf(headline)

      state.headlines[index].complete = true
      state.selectedHeadline = headline
    },
    setShowChoices (state) {
      state.showChoices = true
    },
    setMacbethFinished (state) {
      state.macbethFinished = true
    },
    setNewspaperFinished (state) {
      state.newspaperFinished = true
    },
    enableAudio (state) {
      state.hasAudio = true
    },
    disableAudio (state) {
      state.hasAudio = false
    }
  },
  getters: {
    showNextButton: state => state.headlines.every(headline => headline.complete === true)
  }
})
