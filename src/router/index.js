import Vue from 'vue'
import Router from 'vue-router'
import Introduction from '@/components/Introduction'
import Concept from '@/components/Concept'
import Choice from '@/components/Choice'
import Newspaper from '@/components/Newspaper'
import NewspaperHeadlines from '@/components/NewspaperHeadlines'
import Headline from '@/components/Headline'
import Response from '@/components/Response'
import Intonation from '@/components/Intonation'
import Macbeth from '@/components/Macbeth'
import LadyMacbeth from '@/components/LadyMacbeth'
import Conclusion from '@/components/Conclusion'

Vue.use(Router)

export default new Router({
  base: window.location.pathname,
  routes: [
    {
      path: '/',
      name: 'Introduction',
      component: Introduction,
      meta: {
        title: 'Introduction'
      }
    },
    {
      path: '/concept',
      name: 'Concept',
      component: Concept,
      meta: {
        title: 'Concept'
      }
    },
    {
      path: '/choice',
      name: 'Choice',
      component: Choice,
      meta: {
        title: 'Choice'
      }
    },
    {
      path: '/newspaper',
      name: 'Newspaper',
      component: Newspaper,
      meta: {
        title: 'Newspaper'
      }
    },
    {
      path: '/newspaper-headlines',
      name: 'NewspaperHeadlines',
      component: NewspaperHeadlines,
      meta: {
        title: 'Newspaper Headlines'
      }
    },
    {
      path: '/headline/:id',
      name: 'Headline',
      component: Headline,
      meta: {
        title: 'Headline'
      }
    },
    {
      path: '/response',
      name: 'Response',
      component: Response,
      meta: {
        title: 'Response'
      }
    },
    {
      path: '/intonation',
      name: 'Intonation',
      component: Intonation,
      meta: {
        title: 'Intonation'
      }
    },
    {
      path: '/macbeth',
      name: 'Macbeth',
      component: Macbeth,
      meta: {
        title: 'Macbeth'
      }
    },
    {
      path: '/lady-macbeth',
      name: 'LadyMacbeth',
      component: LadyMacbeth,
      meta: {
        title: 'Lady Macbeth'
      }
    },
    {
      path: '/conclusion',
      name: 'Conclusion',
      component: Conclusion,
      meta: {
        title: 'Conclusion'
      }
    }
  ]
})
