import Vue from 'vue'
import App from '@/App'
import store from '@/store'
import router from '@/router'

require('mdi/scss/materialdesignicons.scss')

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  document.title = `${to.meta.title} | Tone Learning`

  if (to.meta.noAudio) {
    store.commit('disableAudio')
  } else {
    store.commit('enableAudio')
  }

  next()
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App),
  components: { App },
  template: '<App/>'
})
